# Version History

## Version numbering

The software version number has the conventional format: `major-release.minor-release`.

The document version number is: `software-version.document-revision`.

## Releases

### 1.0 (24/08/2022)

This is the initial release of the software.

**Document revision 1.0.2 (24/08/2022)**

> Added information on version numbering.

**Document revision 1.0.1 (24/08/2022)**

> This is the initial revision of the documentation.