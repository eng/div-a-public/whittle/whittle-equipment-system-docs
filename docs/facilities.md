# Facilities

## Main store

The lab's main equipment store is located on the mezzanine above the workshop. 

Items that the system tracks should be stored in the main store by default. Items may be stored elsewhere for convenience. This is recorded by an item's `normallyKeptAt` property, which you can view in the software. This can be edited when the permanent storage of an item is moved from one location to another.

> **Return an item to where it is normally kept.**

## Equipment terminal

![Equipment terminal photo](pictures/main-store-photo.jpg)

In the main store, an equipment terminal can be found. This is a laptop computer running the system's software. There is also a label printer and a barcode scanner. Only on this terminal can a barcode sticker be printed. The same software running on other machines cannot print the barcode. 

The barcode scanner makes looking up an item easy.

> **After registering an item, go to the main store to print the barcode label to be affixed to the item.**
> You need to note down the equipment code assigned at the time of registration to do this.

### Using the label printer for other purposes

It is possible to use the label printer for other purposes: contact Matt Wang (<tw463@cam.ac.uk>) to discuss. The printer's label size is 38 mm by 25 mm and the material is polyethylene which is tear and environmentally resistant. It prints with thermal transfer ribbon which is indelible. The print speed is high: up to 6 inches per second. [Printer datasheet](https://www.zebra.com/content/dam/zebra_new_ia/en-us/solutions-verticals/product/Printers/Industrial%20Printers/ZT200%20Series%20Industrial%20Printers/GENERAL/zt200-spec-sheets/ZT200-Datasheet-English.pdf)
