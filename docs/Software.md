# Software

## Introduction

You need to use the software to access the system.
The software is running on the main store equipment terminal for anyone to use, similarly to a library borrow / return terminal.
Alternatively, you can choose to install the software on your own machine.

## Optional - Installing on your own machine

Installing the software on your own machine helps to avoid travelling to the main store terminal. However, you will not have access to the barcode scanner and label printer, and you cannot print barcode labels using your own copy of the software.

You can install the software if your computer runs 64-bit Windows operating system. Compatibility with other OS is under development.
You need the following runtime environments:

1. [MATLAB Runtime 9.12 (R2022a)](https://uk.mathworks.com/products/compiler/matlab-runtime.html). This is not required if your computer already has MATLAB R2022a installed.  
2. Python 3.8.0. This must be a MATLAB-compatible installation. Specifically, Python installed through Microsoft Stores does not work. Configuration instructions can be found [here](https://uk.mathworks.com/help/matlab/matlab_external/install-supported-python-implementation.html#bujjwjn) and the installer can be downloaded [here](https://www.python.org/downloads/release/python-380/).  
3. Be connected to the CUED network, either directly or through VPN. To check this, try to access this directory through Windows Explorer: `\\wl-srv-hv-03.ad.eng.cam.ac.uk\equipments_share\`. If you are asked for credentials, use username `AD\crsid` and your teaching system password, and make sure to save the credentials.

After setting up these prerequisites, navigate to `\\wl-srv-hv-03.ad.eng.cam.ac.uk\equipments_share\` and copy `wl-sys-v-1-0.zip` to your local machine. Unzip the file to a folder. The `.exe` file is the software's executable file. There is no need to 'install' the software.

> You **MUST NOT** tamper with any other file or directory under `\\wl-srv-hv-03.ad.eng.cam.ac.uk\equipments_share\` as it contains files necessary for the system to function and they are accessed by all running instances of the software.

You should now attempt to run the `.exe` file which is on your local machine. If you do not see the user screen at all, this indicates a problem with your MATLAB Runtime installation. If you encounter an error during start up, this can either be because the software cannot find the Python installation, in which case it will attempt to install Python for you. Or it can be the software cannot access the data server, in which case, repeat step 3. If the problem persists, contact Matt Wang for assistance. 

If you see this user screen, your instance is working properly.

![Main user screen](pictures/main-page.PNG)

## Register an account

You need an account to use the system. The system maintains its own accounts and this is not communicable with the University's or the Engineering Department's. Therefore, you must register a new account.

To register an account, click the '**New User**' button. The following screen should appear. You should fill in your details in all boxes and confirm that you understand how the system works.

> This is a **LOW SECURITY** system. It is designed to be based on **trust**. Password is neither masked nor encrypted. With some knowledge these details can be accessed by other people connected to the CUED network. You should **NOT** use a valuable password, especially **NOT** your Raven or teaching system password.

![User registration screen](pictures/user-registration.PNG)

## Log in and log out

Login is performed in the usual manner by clicking the '**Login**' button on the main user screen and the following login window will appear.

![User login screen](pictures/user-login.PNG)

Once logged in, the main user screen will become 'live' and the functionalities can be accessed. The '**Login**' button becomes '**Logout**' and you can log out by clicking it. The functionalities that are greyed out are either under development, or require administrator privileges to access which your normal account does not have.

![Main user screen live](pictures/main-page-login.PNG)

## Borrow an item

Suppose you have found this item in the store and wish to borrow it.

![Sample equipment](pictures/sample-equipment.jpg)

You firstly notice that it has the equipment code barcode sticker on it, therefore, it is tracked by the system. To borrow this item, log into your account, and click the '**Check Out**' button. A window will appear. If you are using the main store terminal, use the barcode scanner to scan the barcode. This should populate the first line ('Equipment code') of the window. Alternatively, enter the equipment code manually. Press the '**Look up**' button next to the edit field to look up the item. If the equipment code is entered correctly, the item's details will appear.

![Check out window populated with details](pictures/checkout-filled.png)

Verify that this is the item you want. If it is not, press the '**Clear**' button to clear the window and start again. If you wish to borrow this item, press the '**Check out this equipment**' button. Your CRSid will appear in the 'Loaned to' field and the button will become inactive. **You have now borrowed this item.**

> It is possible that the item requires you to have the PI's permission to borrow, or to have a specific briefing to borrow, or both. In these cases, when you press '**Check out this equipment**', a warning window will appear.  
>
> ![Permission level warning](pictures/Capture.PNG)
>
> If you have completed the required actions, press '**OK**' to proceed. Otherwise, press '**Cancel**' and obtain the relevant permission and/or briefing first. Never ignore these warnings.

It is also possible that the item cannot be borrowed when you look it up. This can be because of three reasons:

1. The item cannot be borrowed because its owner set the borrowing permission level to not allow borrowing. Please respect the permission level.
1. The item is on loan to another user. In this case, the current user's CRSid appears in the 'Loaned to' field. If the item is apparently not in use, this can be because it was not returned correctly by the previous user.
2. The item is unserviceable (broken). This will be reflected in the 'Serviceability' field.

## Return an item

To return an item, log into your account and click the '**Check In**' button. A window will appear. If you are using the main store terminal, use the barcode scanner to scan the barcode. This should populate the first line ('Equipment code') of the window. Alternatively, enter the equipment code manually. Press the '**Look up**' button next to the edit field to look up the item. If the equipment code is entered correctly, the item's details will appear.

![Check in window populated with details](pictures/checkin-filled.png)

Verify that this is the correct item to be returned. If it is not, press the '**Clear**' button to clear the window and start again.

> **You do not need to be the person who borrowed an item to return it.** This is to deal with situations such as a student has graduated and their rig needs to be dismantled. Do not abuse this feature to get hold of items that others are using.

You need to update the item's serviceability. The last known serviceability state is pre-selected in the 'Serviceability' selector. If this has changed, select the appropriate state for the item's current status.

Press '**Confirm and Return**' to return the item. The 'Loaned to' field will become blank. The item has now been returned. Please return the physical item to where it is normally kept. If you don't know, return it where you found it or, if all fails, to the main store.

## View the full list of registered items

The system maintains an Excel spreadsheet that contains all registered items. To view this, click the '**View Full Equipment List**' button on the main user screen. Your computer needs to be able to open a spreadsheet in `.xlsx` format.

The spreadsheet is refreshed every night at 2 A.M. and does not necessarily reflect the real time status of items which could change since the last spreadsheet refresh. Administrators have the ability to trigger a refresh manually if necessary.

![Full list example](pictures/spreadsheet-sample.png)

## View or edit a single item by code

If you need to view the details of a specific item or to edit the details, press the '**View / Edit by Code**' button on the main user screen.
Look up the item by entering the equipment and pressing the '**Look up**' button. The following screen will appear. If you need to view a different item, press the '**Clear**' button and start again.

![View edit equipment filled](pictures/view-edit-filled.png)

The basic use of this window is to view the item's details. Additionally, it is used to access the following functionalities:

1. Attachments management. The system provides a directory on the data server for each item to store the attachments, which can be viewed by any user of the system. To access the directory specific to the item, press the '**Attachments**' button. A window will open. An example of using attachments is shown below.
![Attachments example](pictures/attachments-example.png)

2. Print barcode sticker. This is only possible on the main store equipment terminal where the '**Print barcode**' button is active. Each click of the button prints one copy of the item's barcode sticker.

**If you have good reasons to do so**, you can edit the item's details. Fields that you can edit are clickable. Edit the item's details using these fields. You can also change the photo by clicking the '**Change photo**' button which will open a file browser and ask you to select one image which must be of `.jpg` or `.png` format.

After you make the edits, it is essential to write a justification why the edits are necessary, similarly to a 'commit message' in Git. Write this concisely in the '**You can provide further information here**' box. Finally, press the '**Save changes**' button to save the changes. If you have changed the photo, it may take a few seconds to complete the save. Alternatively, press '**Exit without saving**' to abandon all changes. 

If you press '**Clear**' or press close on the top right corner, no change will be saved.

## Register an item

### Who can register an item

Any user of the system can register an item. You do not need to be the owner or PI. However, you should consult the owner / PI with regard to the borrowing permission levels, or leave the settings at default which is to not allow borrowing.

### How to register an item

To add or 'register' an item to be tracked by the system, press the '**Add Equipment**' button on the main user screen. The following window appears. You should populate all fields to the best of your knowledge.

![Add equipment empty](pictures/add-equipment-empty.png)

The '**Equipment Code**' field is not editable as the code is assigned automatically. The code at this stage is **NOT** final. If you change the PI of the item, the code updates accordingly.

A good description is essential. The description should be concise and contains all necessary information to identify the item and determine its use. Information such as the part number, RS number, main parameter (e.g. pressure range), and serial number should be written in the description. 

> Also use the description to indicate defects and other specific information, e.g. 'channel 1 not working', 'has a body and a stand'.

The borrowing permissions are set by the tickboxes. If '**Allow borrowing**' is not ticked, which is the default, the two other settings will not be active. Set the tickboxes to reflect the desired permission level.

It is good practice to include a photo. Press '**Change photo**' to browse for a `.jpg` or `.png` format image for use as the photo. This photo will be displayed when the item is looked up for borrowing or viewing.

When all details are filled correctly, press '**Register**' to register the item. All fields become inactive. It may take a few seconds to process and save the data. If the process is successful, a confirmation box appears. **This confirmation box contains the final equipment code** as registered on the data server. Make a note of this code as necessary. Dismiss the confirmation box. The equipment code shown in the registration form is also updated to be the final code.

### Printing and affixing the barcode

If you are using the main store terminal, the '**Print barcode**' button will become active once registration is successful. Click this button to print the item's barcode sticker. Each click prints one copy. If you are using another computer, it is necessary to go to the main store and use the [view equipment](#view-or-edit-a-single-item-by-code) functionality to print the barcode. Do the same if you are using the main store terminal but forgot to print barcode at registration time.

Affix the barcode sticker securely to a conspicuous location on the item. Use multiple stickers as necessary / for large items / for multi-component items.

## Administrator privileges

A normal user can be granted administrator privileges. This gives the ability to:

1. [Edit the fields that are greyed out](#view-or-edit-a-single-item-by-code), including borrowing permission levels of an item.
2. Manually refresh the [Excel spreadsheet](#view-the-full-list-of-registered-items).
3. Manually backup all data.
4. Shutdown the software instance running on the main store equipment terminal.

Point one is the one that is of use to most people requiring admin privileges. If you wish to require admin privileges, contact Matt Wang.

## Technical

The software is written in MATLAB R2022a. 

It is necessary to have a MATLAB-compatible Python installation because the software makes runtime calls to a number of Python scripts to allow the administrators to make changes to the system without updating everyone's binary `.exe` files. Python needs to be used because MATLAB Runtime lacks the interpreter so MATLAB commands such as `eval` and `run` cannot be used in the deployed environment. The software also calls a Python script to generate the binary files for the label printer.

### Licence

MIT Licence applies to the executable. The source code can be obtained from Matt Wang on a case-by-case basis.

Copyright 2021-2022 Matt Wang, Whittle Laboratory

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.