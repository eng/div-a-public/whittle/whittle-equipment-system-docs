# Introduction

**This is a live document.**
Do not save it locally. Always view the most up-to-date copy here.

## Release information

Software: version 1.0  
Released: 24/08/2022 

Documents: version 1.0.2   
Released: 24/08/2022

Blame and direct all queries to Matt Wang (<tw463@cam.ac.uk>)

## The Whittle Equipment System for the impatient

1. To start using the system, go to the terminal in the main store (or install the software on your own machine) and register an account.  
2. To borrow an item, use the 'check out' functionality to book it to your name.  
3. When you're done, use the 'check in' functionality to return an item. Update the state of serviceability when you check in, and return the item to where it is normally kept.
4. To add and edit items and for other functionalities, read the full documentation or come to Matt Wang and I can run you through.

## What is the system

The system is a set of hardware, software, procedures, and best practices to facilitate finding, sharing, and book keeping of our equipment.
It delivers the basic inventory functionalities with the **minimum amount of effort from the users**.

As a first step, the system aims to bridge the gap between the user and the spare equipment around the lab. This accelerates research progress, reduces the amount of clutter, and saves money.

The system aims to progressively expand in scope and finally track all items of equipment and consumable in the lab. 
The vision is to combine the roles of an inventory system (for equipment) and a part number system (for consumables). 
__This needs your support. Please promote its usage where you can.__

### The system relies on everyone's effort

Having the system alone is useless if people ignore it and carry on with the old ways of taking items from open shelves and forgetting about them when done. To keep the system effective, please can we ask that everyone follows a few basic rules:  

- **Don't ignore the system.** If you need to use it to access something, please do so.  
- Respect the borrowing permission levels.  
- Take care to make true and accurate entries in the software.   
- Return items to where they are normally kept.  

## Who can use the system

Anyone working in the Whittle lab can use the system by creating an account. This includes those who are working on a short-term project.
We hope this will increase the amount of usage and accelerate the adoption.

Therefore, 4th years and UROP students are not exempt from following the correct access procedures to use shared equipment that is tracked by the system.
PIs can help by keeping them informed.

## When to use the system

For owners of equipment, using the system is voluntary and there is no compulsion to register your items. However, you are strongly encouraged to do so even if you do not intend to share them. Being registered on the system does not automatically make an item liable to being shared (and taken by other people) - see [Borrowing Permissions](#borrowing-permissions).

> Registering non-sharing items have the following benefits:
>
> - Access the system's bookkeeping functionalities, such as the Attachments function to store files such as quotes, calibration, certificate of conformity.  
> - Facilitate sharing the item's information among collaborating researchers.  
> - Let others see what you use for your experiments.  
> - Promote using the system by setting an example.  

On the other hand, if you wish to borrow an item that is tracked by the system, you **must** use the system and follow the correct process to access the item. The system relies on all of us to follow the [basic rules](#the-system-relies-on-everyones-effort) to work.

> To determine whether an item is being tracked by the system, look for a [six digit plus letters code marked on the item](#equipment-code).
> This is normally in the format of a barcode sticker depicted below.
>
> ![sample barcode](pictures/sample-barcode.png)    

## Equipment code

### Format

Any item that is being tracked by the system has a unique identifier code. The code comprises a six digit unique serial (from 000001 to 999999) and the PI's initials.

For example: `012345RJM` represents an item belonging to Rob Miller. The available PI codes are (in no particular order): `AJW`, `NRA`, `RJM`, `JPL`, `JVT`, `GP`, `LPX`, `WND`, `CAH`, and `IJD`.

> For items not belonging to a particular PI, there are four categorical codes:  
>
> - LAB (`012345LAB`) - for items belonging to the lab, or items
where the PI has retired and the ownership is passed onto the lab in general, or items where the PI cannot be determined.  
> - WKSP (`012345WKSP`) - for items belonging to the workshop.  
> - CDT (`012345CDT`) - Centre for Doctoral Training.  
> - AIA (`012345AIA`) - Aviation Impact Accelerator.  

### Request a PI code

If you are not an academic staff but would find it useful to have your own PI code, or if you want to have a categorical code, please get in touch with Matt Wang (<tw463@cam.ac.uk>).

### Assignment

The equipment code is assigned at the time of registration. The serial is assigned incrementally and the PI code is appended. 

Once assigned, the equipment code will not change through the item's lifecycle. For example, if the ownership of item `012345RJM` is passed onto another person, the equipment code will remain `012345RJM`. Because of this, it is safe to mark the equipment code on the item permanently.

If an item is removed from the system, the serial is not recycled.

### Bar code

A bar code sticker encoding the equipment code (e.g. `012345RJM`) should be affixed to any item that is tracked by the system. This sticker can be printed on the main store terminal machine. The bar code makes it easy to look up an item on the terminal by scanning the barcode rather than manually entering the equipment code.

## Borrowing permissions

There are three settings that control an item's borrowing permission level. They are set at the time of registration and only administrators can change these settings later on.

**Allow borrowing** (`allowBorrowing`)  
> If set to `false`, the item cannot be borrowed, and the following two settings do not apply. The default is `false`.

**Seek PI permission before borrowing** (`seekPIPermissionToBorrow`)  
> As the name suggests, this setting requires the user to seek the PI's permission to borrow an item. The default is `false`.

**Briefing to be had before borrowing** (`seekBriefingToBorrow`)
> Requires the user to have a specific briefing before borrowing an item. This is applicable to items that require user competence, e.g. workshop power tools or delicate instruments. The default is `false`.

The latter two settings trigger prompts at the time of borrowing to ask the user to confirm they have completed the necessary actions. The system has no way of policing it and it relies on people following the [basic rules](#the-system-relies-on-everyones-effort).

## Acknowledgements

Thanks goes to Dom and Jordi for their work in preparing the physical and IT facilities that make this system possible.
